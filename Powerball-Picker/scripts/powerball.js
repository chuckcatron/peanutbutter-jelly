// JavaScript Document

// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {
    getLocation();
}

function getLocation() {
    navigator.geolocation.getCurrentPosition(onGeolocationSuccess, onGeolocationError);
}

/**
 * Returns a random integer between min and max
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setOutput(message)
{
    var powerballNumbers = document.getElementById('powerballNumbers');    
    powerballNumbers.innerHTML = powerballNumbers.innerHTML + message;
}

function clearOutput()
{
    $("#number1").text('');
    $("#number2").text('');
    $("#number3").text('');
    $("#number4").text('');
    $("#number5").text('');
    $("#powerBallNumber").text('');
}

function setBoom(message)
{
    var boomNumber = document.getElementById('boomNumber');    
    boomNumber.innerHTML = message;
    boomNumber.style.display = 'block';
}

function timer()
{
    boomNumber.style.display = 'none';
}

//=======================Say Hello (Page 1) Operations=======================//
function getNumbers() {
    clearOutput();
    var rndNumbers = [];
    var num = 0;
    for(var i = 0; i <= 4; i++)
    {
        var a = 0;
        while(a >= 0){
            num = getRandomInt(1, 60);
            a = rndNumbers.indexOf(num);
        }
        
        rndNumbers.push(num);   
    }
    
    $("#number1").text('#');
    $("#number1").text(rndNumbers[0]);   

    $("#number2").text('#');
    $("#number2").text(rndNumbers[1]);

    $("#number3").text('#');
    $("#number3").text(rndNumbers[2]);  
    
    $("#number4").text('#');
    $("#number4").text(rndNumbers[3]); 
    
    $("#number5").text('#');
    $("#number5").text(rndNumbers[4]);

    num = getRandomInt(1, 35);
    $("#powerBallNumber").text('#');
    $("#powerBallNumber").text(num);
}
    
function getNumbersReset() {
    clearOutput();
}

function GetPBInfo() {
    $("#pbDate").text('');
    $("#pbText").text('');
    var divToBeWorkedOn = "#pbDate";
    var webMethod = "http://wsf.cdyne.com/WeatherWS/Weather.asmx/GetCityWeatherByZIP?ZIP=45322";
    var parameters = "{'ZIP':'" + 45322 + "'}";
    //alert("Calling: " + webMethod);
    //http://kyleschaeffer.com/best-practices/the-perfect-jquery-ajax-request/
    $.ajax({
      type: 'POST',
      //url: 'http://kyleschaeffer.com/feed/',
      url: 'http://wsf.cdyne.com/WeatherWS/Weather.asmx/GetCityWeatherByZIP',  
      data: { ZIP: '45322' },
      beforeSend:function(){
        // this is where we append a loading image
        $('#ajax-panel').html('<div class="loading"><img src="styles/images/loading.gif" alt="Loading..." /></div>');
      },
      success:function(data){
        // successful request; do something with the data
        $('#ajax-panel').empty();
          //alert(data);
        $(data).find('WeatherReturn').each(function(i){
            $('#ajax-panel').append('<p>Location: ' + $(this).find('City').text() + ', ' + $(this).find('State').text() + '</p>');
            $('#ajax-panel').append('<p>Current conditions: ' + $(this).find('Description').text() + '</p>');
            $('#ajax-panel').append('<p>Current temperature: ' + $(this).find('Temperature').text() + '</p>');
        });
      },
      error:function(){
        // failed request; give feedback to user
        $('#ajax-panel').html('<p class="error"><strong>Oops!</strong> Try that again in a few moments.</p>');
      }
    });
}

function OnGetPBDateSuccess(data, status) {
    alert("Data: " + data + " Status: " + status);
}

function OnGetPBDateError(request, status, error) {
    alert("Error: " + error + " Request: " + request + " Status: " + status);
}
//=======================Geolocation Operations=======================//
// onGeolocationSuccess Geolocation
function onGeolocationSuccess(position) {
    // Use Google API to get the location data for the current coordinates
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    geocoder.geocode({ "latLng": latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if ((results.length > 1) && results[1]) {
                $("#myLocation").html(results[1].formatted_address);
            }
        }
    });

    // Use Google API to get a map of the current location
    // http://maps.googleapis.com/maps/api/staticmap?size=280x300&maptype=hybrid&zoom=16&markers=size:mid%7Ccolor:red%7C42.375022,-71.273729&sensor=true
    var googleApis_map_Url = 'http://maps.googleapis.com/maps/api/staticmap?size=300x300&maptype=hybrid&zoom=16&sensor=true&markers=size:mid%7Ccolor:red%7C' + latlng;
    var mapImg = '<img src="' + googleApis_map_Url + '" />';
    $("#map_canvas").html(mapImg);
}

// onGeolocationError Callback receives a PositionError object
function onGeolocationError(error) {
    $("#myLocation").html("<span class='err'>" + error.message + "</span>");
}